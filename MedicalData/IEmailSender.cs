﻿using System.Threading.Tasks;

namespace MedicalData
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
