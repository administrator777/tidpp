﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MedicalData.Models;
using Microsoft.AspNetCore.Identity;

namespace MedicalData
{
    public interface IDoctor
    {
        void Add(Doctor newDoctor);
        void AddAsync(Doctor newDoctor);
        void DeleteDoctor(int doctorId);
        Task<bool> DoctorExistAsync(int doctorId);

        Task<IEnumerable<ClientOrderAndTime>> GetAllClientTimeFromDoctorAsync(int doctorId);
        IEnumerable<Doctor> GetAllDoctors();

        Task<IEnumerable<Doctor>> GetAllDoctorsAsync();

        Doctor GetDoctor(string fullName);
        Doctor GetDoctor(int doctorId);
        Task<Doctor> GetDoctorAsync(string fullName);
        Task<Doctor> GetDoctorAsync(int doctorId);

        int GetDoctorIdAtFullName(string fullName);
        Task<int> GetDoctorIdAtFullNameAsync(string fullName);

        string GetDoctorName(int doctorId);
        Task<string> GetDoctorNameAsync(int doctorId);

        Task<IEnumerable<WorkTime>> GetDoctorWorkingTimeAsync(int doctorId);
        void UpdateAsync(Doctor doctor);
    }
}