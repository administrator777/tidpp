﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MedicalData.Models
{
    public class Office
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Limit branch name to 20 characters")]
        public string Name { get; set; }

        public string Adress { get; set; }

        public string Telephone { get; set; }

    }
}
