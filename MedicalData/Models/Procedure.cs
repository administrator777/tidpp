﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalData.Models
{
    public class Procedure
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TimeSpan DurationTime { get; set; }
        public string Information { get; set; }
        public int Price { get; set; }
    }
}
