﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;

namespace MedicalData.Models
{
    public class WorkTime

    {
        [Key]
        public int Id { get; set; }
        public TimeSpan StartWorkingDay { get; set; }
        public TimeSpan EndWorkingTime { get; set; }

        public int DayNumber { get; set; }
    }
}
