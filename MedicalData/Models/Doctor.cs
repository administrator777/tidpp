﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MedicalData.Models
{
   public class Doctor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string UrlImage { get; set; }
        public DateTime BirthDay { get; set; }
        public string Information { get; set; }
        public IEnumerable<WorkTime> WorkTime { get; set; }
        public virtual ApplicationUser User { get; set; }

    }
}
