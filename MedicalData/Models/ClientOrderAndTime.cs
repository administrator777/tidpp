﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalData.Models
{
    public class ClientOrderAndTime
    {
        public DateTime DateTimeSince { get; set; }
        public TimeSpan DurationProcedure { get; set; }
    }
}
