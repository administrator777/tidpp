﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MedicalData.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime RecordTime { get; set; }
        public Doctor Doctor { get; set; }
        public Procedure Procedure { get; set; }
        public string Comment { get; set; }
        public string Telephone { get; set; }
    }
}
