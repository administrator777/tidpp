﻿using System;
using System.Linq;
using MedicalData.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MedicalData
{
    public class MedicalContext : IdentityDbContext<ApplicationUser>
    {
        public MedicalContext(DbContextOptions<MedicalContext> options) : base(options)
        {
       
        }


//        public MedicalContext(DbContextOptions<MedicalContext> options)
//            : base(options)
//        {
//        }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Procedure> Procedures { get; set; }
        public DbSet<WorkTime> WorkTimes { get; set; }
        public DbSet<Client> Client { get; set; }

    }
}
