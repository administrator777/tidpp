﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MedicalData.Models;

namespace MedicalData
{
    public interface IProcedure
    {
        void Add(Procedure newProcedure);
        void AddAsync(Procedure newProcedure);

        IEnumerable<Procedure> GetAllProcedures();
        Task<IEnumerable<Procedure>> GetAllProceduresAsync();

        TimeSpan GetDurationProcedure(string procedureName);
        Task<TimeSpan> GetDurationProcedureAsync(string procedureName);

        Procedure GetProcedure(string name);
        Task<Procedure> GetProcedureAsync(string name);

        Procedure GetProcedureById(int id);
        Task<Procedure> GetProcedureByIdAsync(int id);
    }
}