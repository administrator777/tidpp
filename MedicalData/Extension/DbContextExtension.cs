﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MedicalData.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MedicalData.Extension
{
    public static class DbContextExtension
    {

        public static bool AllMigrationsApplied(this DbContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }

        public static void EnsureSeeded(this MedicalContext context)
        {

            if (!context.Doctors.Any())
            {
                var doctor = new Doctor
                {
                    Id = 1,
                    FirstName = "Donald",
                    Lastname = "Duck",
                    WorkTime = new List<WorkTime>()
                    {
                        new WorkTime
                        {

                            Id = 1,
                            DayNumber = 0,
                            StartWorkingDay = new TimeSpan(8, 0, 0),
                            EndWorkingTime = new TimeSpan(18, 0, 0)
                        }
                    }
                };
                context.Doctors.Add(doctor);
                context.SaveChanges();
            }

           
        }


    }
}
