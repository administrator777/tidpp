﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MedicalData.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace MedicalData
{
    public static class DbInitializer
    {


        public static async void Seed(IClient context)
        {
            //I'm bombing here
            if (!context.CheckIfUserExist())
            {
                var email = "medical@gmail.com";
                var user = new ApplicationUser
                {

                };

                if (!await context.CheckUserExistAsync(user.UserName))
                {
                    var password = new PasswordHasher<ApplicationUser>();
                    var hashed = password.HashPassword(user, "Asdfgh123#");
                    user.PasswordHash = hashed;
                    var result = await context.AddUserAsync(user);
                    //                    var userStore = new UserStore<ApplicationUser>(context);
                    //                    var result = await userStore.CreateAsync(user);
                    //                    await userStore.AddToRoleAsync(user, "ADMINISTRATOR");
                    //                    context.SaveChanges();

                    var st = 0;
                }


            }
        }

        public static void CreateRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roles = { "Administrator", "Doctor", "Client", "Guest" };
            foreach (var role in roles)
            {
                Task<bool> hasRole = roleManager.RoleExistsAsync(role);
                hasRole.Wait();
                if (!hasRole.Result)
                {
                    var roleResult = roleManager.CreateAsync(new IdentityRole(role));
                    roleResult.Wait();
                }
            }
        }



        public static void AddAdministrator(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string email = "medical@gmail.com";
            string password = "Asdfgh123#";

            //Check that there is an Administrator role and create if not
            Task<bool> hasAdminRole = roleManager.RoleExistsAsync("Administrator");
            hasAdminRole.Wait();

            if (!hasAdminRole.Result)
            {
                var roleResult = roleManager.CreateAsync(new IdentityRole("Administrator"));
                roleResult.Wait();
            }

            //Check if the admin user exists and create it if not
            //Add to the Administrator role

            Task<ApplicationUser> testUser = userManager.FindByEmailAsync(email);
            testUser.Wait();

            if (testUser.Result == null)
            {
                ApplicationUser administrator = new ApplicationUser
                {
                    Email = email,
                    NormalizedEmail = email.ToUpper(),
                    UserName = email,
                    NormalizedUserName = email.ToUpper(),
                    PhoneNumber = "+76781882",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                };

                Task<IdentityResult> newUser = userManager.CreateAsync(administrator, password);
                newUser.Wait();

                if (newUser.Result.Succeeded)
                {
                    Task<IdentityResult> newUserRole = userManager.AddToRoleAsync(administrator, "Administrator");
                    newUserRole.Wait();
                }
            }

        }

        public static void AddProcedures(IServiceProvider serviceProvider)
        {

            #region AddProcedure And Doctor

            var context = serviceProvider.GetRequiredService<MedicalContext>();
            if (!context.Procedures.Any())
            {
                context.Procedures.Add(new Procedure
                {
                    DurationTime = new TimeSpan(0, 20, 0),
                    Name = "Endodontic",
                    Price = 200,
                    Information =
                        "Pulpotomy — the opening of the pulp chamber of the tooth to allow an infection to drain; usually a precursor to a root canal"
                });
                context.Procedures.Add(new Procedure
                {
                    DurationTime = new TimeSpan(0, 30, 0),
                    Name = "Prosthodontics",
                    Price = 200,
                    Information =
                        "Crown (caps) — artificial covering of a tooth made from a variety of biocompatible materials, including CMC/PMC (ceramic/porcelain metal composite), gold or a tin/aluminum mixture. The underlying tooth must be reshaped to accommodate these fixed restorations"
                });
                context.Procedures.Add(new Procedure
                {
                    DurationTime = new TimeSpan(0, 20, 0),
                    Name = "Orthodontic ",
                    Price = 200,
                    Information =
                        "Implants and implant-supported prosthesis — also an orthodontic treatment as it involves bones"
                });
                context.Procedures.Add(new Procedure
                {
                    DurationTime = new TimeSpan(0, 20, 0),
                    Name = "Periodontics ",
                    Price = 200,
                    Information = "a procedure to sever the fibers around a tooth, preventing it from relapsing"
                });
                context.SaveChanges();
            }

            #endregion

        }

        public static void AddDoctors(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var context = serviceProvider.GetRequiredService<MedicalContext>();
            if (!context.Doctors.Any())
            {
                var email = "michael.robbins@gmail.com";
                var password = "Asdfgh123#";
                var doctorUser = new ApplicationUser
                {
                    Email = email,
                    NormalizedEmail = email.ToUpper(),
                    UserName = email,
                    NormalizedUserName = email.ToUpper(),
                    PhoneNumber = "+76781882",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    FirstName = "Michael",
                    LastName = "Robbins"
                };

                Task<IdentityResult> newUser = userManager.CreateAsync(doctorUser, password);
                newUser.Wait();

                if (newUser.Result.Succeeded)
                {
                    Task<IdentityResult> newUserRole = userManager.AddToRoleAsync(doctorUser, "Doctor");
                    newUserRole.Wait();
                }

                context.Doctors.Add(new Doctor
                {
                    FirstName = "Michael",
                    Lastname = "Robbins",
                    BirthDay = new DateTime(1975, 10, 1),
                    Information =
                        "If you have neck or back pain, this is where you need to go! Dr. Michael Robbins is the best chiropractor in Markham. His knowledge, skills and patience is second to.",
                    UrlImage = "/doctors/doctor1.jpg",
                    User = doctorUser,
                    WorkTime = new[]
                    {
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                        }
                });
                context.Doctors.Add(new Doctor
                {
                    FirstName = "David",
                    Lastname = "Warketin",
                    BirthDay = new DateTime(1990, 10, 1),
                    Information =
                        "I've been having problems with colitis.I called Dr.Warkentin at 10pm it was so bad.Not only did he pick up but was very gracious and understanding. ",
                    UrlImage = "/doctors/doctor2.jpg",
                    WorkTime = new[]
                    {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                });
                context.Doctors.Add(new Doctor
                {
                    FirstName = "Bruce",
                    Lastname = "Hoffman",
                    BirthDay = new DateTime(1960, 10, 1),
                    Information =
                        "DR.HOFFMAN TREATED ME AND MY FELLOW MARINE FOR PTSD. DR. HOFFMAN TAUGHT US HOW TO CONTROL OUT THOUGHTS",
                    UrlImage = "/doctors/doctor3.jpg",
                    WorkTime = new[]
                    {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                });
                context.Doctors.Add(new Doctor
                {
                    FirstName = "Tassos",
                    Lastname = "Irikinas",
                    BirthDay = new DateTime(1975, 10, 1),
                    Information =
                        "Great experience recently. Liked the personal touch with his email. Expecting-I hope- a result similar to the ones mentioned on this site. ",
                    UrlImage = "/doctors/doctor4.jpg",
                    WorkTime = new[]
                    {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                });
                context.Doctors.Add(new Doctor
                {
                    FirstName = "Jose",
                    Lastname = "Rodriquez",
                    BirthDay = new DateTime(1960, 10, 1),
                    Information =
                        "Le dr Rodriguez m'a opérée il y a 3 mois pour une hernie incisionnelle. C'est un chirurgien d'une compétence et d'une humanité hors du commun",
                    UrlImage = "/doctors/doctor5.jpg",
                    WorkTime = new[]
                    {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },

                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },

                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                });
                context.Doctors.Add(new Doctor
                {
                    FirstName = "Michael",
                    Lastname = "Robbins",
                    BirthDay = new DateTime(1975, 10, 1),
                    Information =
                        "If you have neck or back pain, this is where you need to go! Dr. Michael Robbins is the best chiropractor in Markham. His knowledge, skills and patience is second to.",
                    UrlImage = "/doctors/doctor1.jpg",
                    WorkTime = new[]
                    {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                });
                context.SaveChanges();
            }
        }
        //        public static async void Seed(IApplicationBuilder applicationBuilder)
        //        {
        //            //I'm bombing here
        //            MedicalContext context = applicationBuilder.ApplicationServices.GetRequiredService<MedicalContext>();
        //
        //            if (!context.Users.Any())
        //            {
        //                var email = "medical@gmail.com";
        //                var user = new ApplicationUser
        //                {
        //                    Email = email,
        //                    NormalizedEmail = email.ToUpper(),
        //                    UserName = email,
        //                    NormalizedUserName = email.ToUpper(),
        //                    PhoneNumber = "+76781882",
        //                    EmailConfirmed = true,
        //                    PhoneNumberConfirmed = true,
        //                    SecurityStamp = Guid.NewGuid().ToString("D")
        //                };
        //
        //                if (!context.Users.Any(u => u.UserName == user.UserName))
        //                {
        //                    var password = new PasswordHasher<ApplicationUser>();
        //                    var hashed = password.HashPassword(user, "Asdfgh123#");
        //                    user.PasswordHash = hashed;
        //                    var userStore = new UserStore<ApplicationUser>(context);
        //                    var result = await userStore.CreateAsync(user);
        //                    await userStore.AddToRoleAsync(user, "ADMINISTRATOR");
        //                    var st = 0;
        //                    //                        if (result.Succeeded)  context.SaveChanges();
        //                }
        //                //
        //                //                    var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
        //                //                    await userManager.AddToRoleAsync(user, "Administrator");
        //
        //
        //                //                    await AssignRoles(serviceProvider, user.Email, roles);
        //            }
        //
        //            context.SaveChanges();
        //        }


    }
}