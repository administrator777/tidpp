﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalData.Models;
using Microsoft.AspNetCore.Identity;

namespace MedicalData
{
    public interface IClient
    {
        void Add(Client newClient);
        void Add(Doctor newDoctor);
        void Add(Procedure newProcedure);
        void AddAsync(Client newClient);
        void AddAsync(Doctor newDoctor);
        void AddAsync(Procedure newProcedure);


        //Seed
        Task<IdentityResult> AddUserAsync(ApplicationUser user);

        bool CheckIfUserExist();

        Task<bool> CheckUserExistAsync(string username);
        IEnumerable<Client> GetAll();

        Task<IEnumerable<ClientOrderAndTime>> GetAllClientTimeFromDoctor(int doctorId);
        IEnumerable<Doctor> GetAllDoctors();

        Task<IEnumerable<Doctor>> GetAllDoctorsAsync();

        IEnumerable<Procedure> GetAllProcedures();
        Task<IEnumerable<Procedure>> GetAllProceduresAsync();
        string GetComment(int id);

        DateTime GetDateTime(int id);

        //        string GetDoctorName(int id);
        Doctor GetDoctor(string name);

        Task<Doctor> GetDoctorAsync(string name);

        //        int GetDoctorIdAtFullName(string fullName);
        Task<int> GetDoctorIdAtFullName(string fullName);

        //End Seed
        Task<string> GetDoctorName(int id);

        Task<IEnumerable<Doctor>> GetDoctors();


        Task<IEnumerable<WorkTime>> GetDoctorWorkingTimeAsync(int doctorId);

        TimeSpan GetDurationProcedure(string procedureName);
        Task<TimeSpan> GetDurationProcedureASync(string procedureName);
        Task<string> GetEmailAsync(int id);
        Procedure GetProcedure(string name);

        Task<Procedure> GetProceduresById(int id);
        IQueryable<Client> GettAllQuerable();
    }
}