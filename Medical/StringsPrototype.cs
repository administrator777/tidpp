﻿namespace Medical
{
    public class StringsPrototype
    {
        #region Controller

        public static string ControllerString =
            @"using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using <context_directory>;
using <context_directory>.Models;


namespace <namespace>
{
     [Route(""[controller]/[action]"")]
    public class <controller_name> : Controller
    {
        private readonly <context_name> _context;
        
        public <controller_name>(<context_name> context)
        {
            _context = context;
        }
        
        //GET: <context_table>
        public async Task<IActionResult> Index()
        {
            return View(await _context.<context_table>.ToListAsync());
        }
        
        // GET: <context_table>/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            System.Diagnostics.Debugger.Break();
            return RedirectToAction(""Index"", ""Home"");

            if (id == null)
            {
                return NotFound();
            }

            var data = await _context.<context_table>
                .SingleOrDefaultAsync(m => m.<id> == id);
            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }

        // GET: <context_table>/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: <context_table>/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind(""<bind_data>"")] <context_modelName> data)
        {
            if (ModelState.IsValid)
            {
                _context.Add(data);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(data);
        }


        // GET: <context_table>/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var data = await _context.<context_table>.SingleOrDefaultAsync(m => m.<id> == id);
            if (data == null)
            {
                return NotFound();
            }
            return View(data);
        }

        // POST: <context_table>/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind(""<bind_data>"")] <context_modelName>  data)
        {
            if (id != data.<id>)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(data);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientExists(data.<id>))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(data);
        }


        // GET: <context_table>/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var data = await _context.<context_table>
                .SingleOrDefaultAsync(m => m.<id> == id);
            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }


        // POST: <context_table>/Delete/5
        [HttpPost, ActionName(""Delete"")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var data = await _context.<context_table>.SingleOrDefaultAsync(m => m.<id> == id);
            _context.<context_table>.Remove(data);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

         private bool ClientExists(int id)
        {
            return _context.<context_table>.Any(e => e.<id> == id);
        }
    }
}";

        #endregion

        #region CreateView

        public static string CreateViewPrototype = @"
            <div class=""form-group"">
                <label asp-for=""<model_row>"" class=""control-label""></label>
                <input asp-for=""<model_row>"" class=""form-control"" />
                <span asp-validation-for=""<model_row>"" class=""text-danger""></span>
            </div>
";

        public static string CreateView =
            @"@model <model_data>
@{
    ViewData[""Title""] = ""Create"";
    Layout = ""<layout_directory>"";
}
<h2>Create</h2>
<h4>Client</h4>
<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <form asp-action=""Create"">
            <div asp-validation-summary=""ModelOnly"" class=""text-danger""></div>

            <injection_data>

            <div class=""form-group"">
                <input type=""submit"" value=""Create"" class=""btn btn-default"" />
            </div>
        </form>
    </div>
</div>

<div>
    <a asp-action=""Index"">Back to List</a>
</div>
";

        #endregion

        #region DeleteView

        public static string DeleteViewPrototype = @"
        <dt>
            @Html.DisplayNameFor(model => model.<model_row>)
        </dt>
        <dd>
            @Html.DisplayFor(model => model.<model_row>)
        </dd>";

        public static string DeleteView = @"
@model <model_data>
@{
    ViewData[""Title""] = ""Delete"";
    Layout = ""<layout_directory>"";
}
<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>Client</h4>
    <hr />
    <dl class=""dl-horizontal"">
        
            <injection_data>



    </dl>
    
    <form asp-action=""Delete"">
        <input type=""hidden"" asp-for=""Id"" />
        <input type=""submit"" value=""Delete"" class=""btn btn-default"" /> |
        <a asp-action=""Index"">Back to List</a>
    </form>
</div>
";

        #endregion

        #region DetailsView

        public static string DetailsViewPrototype = @"
        <dt>
            @Html.DisplayNameFor(model => model.<model_row>)
        </dt>
        <dd>
            @Html.DisplayFor(model => model.<model_row>)
        </dd>
";

        public static string DetailsView = @"
@model <model_data>
@{
    ViewData[""Title""] = ""Details"";
    Layout = ""<layout_directory>"";
}
<div>
    <h4>Client</h4>
    <hr />
    <dl class=""dl-horizontal"">
         <injection_data>
    </dl>
</div>
<div>
    <a asp-action=""Edit"" asp-route-id=""@Model.<id>"">Edit</a> |
    <a asp-action=""Index"">Back to List</a>
</div>
";

        #endregion

        #region EditView      

        public static string EditViewPrototype = @"
            <div class=""form-group"">
                <label asp-for=""<model_row>"" class=""control-label""></label>
                <input asp-for=""<model_row>"" class=""form-control"" />
                <span asp-validation-for=""<model_row>"" class=""text-danger""></span>
            </div>
";

        public static string EditView = @"
@model <model_data>
@{
    ViewData[""Title""] = ""Edit"";
    Layout = ""<layout_directory>"";
}

<h2>Edit</h2>

<h4>Client</h4>
<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <form asp-action=""Edit"">
            <div asp-validation-summary=""ModelOnly"" class=""text-danger""></div>
            <input type=""hidden"" asp-for=""Id"" />

   <injection_data>

<div class=""form-group"">
                <input type=""submit"" value=""Save"" class=""btn btn-default"" />
            </div>
        </form>
    </div>
</div>

<div>
    <a asp-action=""Index"">Back to List</a>
</div>

@section Scripts {
    @{await Html.RenderPartialAsync(""_ValidationScriptsPartial"");}
}
";

        #endregion

        #region IndexView

        public static string IndexViewPrototypeHead = @"
                <th>
                    @Html.DisplayNameFor(model => model.<model_row>)
                </th>
";

        public static string IndexViewPrototypeBody = @"
            <td>
                 @Html.DisplayFor(modelItem => item.<model_row>)
            </td>
";

        public static string IndexView = @"
@model IEnumerable<<model_data>>
@{
    ViewData[""Title""] = ""Index"";
    Layout = ""<layout_directory>"";
}

<h2>Index</h2>

<p>
    <a asp-action=""Create"">Create New</a>
</p>

<table class=""table"">
    <thead>
        <tr>
            <injection_data>
        </tr>
    </thead>
    <tbody>
@foreach (var item in Model) {
        <tr>
            <injection_body>
            <td>
                <a asp-action=""Edit"" asp-route-id=""@item.<id>"">Edit</a> |
                <a asp-action=""Details"" asp-route-id=""@item.<id>"">Details</a> |
                <a asp-action=""Delete"" asp-route-id=""@item.<id>"">Delete</a>
            </td>
        </tr>
}
    </tbody>
</table>

";

        #endregion
    }
}