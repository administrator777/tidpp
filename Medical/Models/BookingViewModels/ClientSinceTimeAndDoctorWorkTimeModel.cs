﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalData.Models;

namespace Medical.Models.BookingViewModels
{
    public class ClientSinceTimeAndDoctorWorkTimeModel
    {
        public IEnumerable<ClientOrderAndTime> ClientOrderTime { get; set; }
        public IEnumerable<WorkTime> DoctorWorkTime { get; set; }
        public TimeSpan ProcedureDuration { get; set; }
    }
}
