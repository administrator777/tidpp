﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Medical.Models.BookingViewModels
{
    public class ClientIndexListingModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string DoctorName { get; set; }
        public string Procedure { get; set; }
        public DateTime DateTime { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public string Telephone { get; set; }
        public DateTime RecordTime { get; set; }
    }


//    public class ClientIndexListingModel
//    {
//        public int Id { get; set; }
//        public string FirstName { get; set; }
//        [Required]
//        public string LastName { get; set; }
//        public string DoctorName { get; set; }
//        public string Procedure { get; set; }
//        public DateTime DateTime { get; set; }
//        public string Email { get; set; }
//        public string Comment { get; set; }
//        public string Telephone { get; set; }
//        public DateTime RecordTime { get; set; }
//    }


}
