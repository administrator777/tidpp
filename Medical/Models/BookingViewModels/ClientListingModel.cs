﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Medical.Models.BookingViewModels
{
    public class ClientListingModel
    {
        public IEnumerable<Task<ClientIndexListingModel>> Clients { get; set; }
    }
}
