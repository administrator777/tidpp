﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MedicalData.Models;

namespace Medical.Models.HomeViewModels
{
    public class DoctorAndNamesViewModel
    {
        public IEnumerable<Task<DoctorName>> DoctorsNames { get; set; }
        public IEnumerable<ProcedureName> ProceduresName { get; set; }
    }

    public class DoctorName
    {
        public string FullName { get; set; }
    }


    public class ProcedureName
    {
        public string Name { get; set; }
    }

    public class DoctorAndProcedureNames
    {
        public string DoctorFullName { get; set; }
        public string ProcedureFullName { get; set; }
    }

    public class WorkingTimeandDays
    {
        public IEnumerable<WorkTime> WorkingDays { get; set; }
        public IEnumerable<DateTime> ClientOrder { get; set; }
    }


}
