﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Medical.Models.HomeViewModels
{
    public class ClientOrderModel
    {
        [Required]
        public string ProcedureName { get; set; }

        [Required]
        public string DoctorName { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateTimeSince { get; set; }

        [Required]
        [RegularExpression("^\\s*(\\S+\\s+|\\S+$){2,3}$",ErrorMessage ="Please enter your FirstName and your LastName")]
        public string FullName { get; set; }

        [Required(ErrorMessage ="Required Phone Number")]
        [DataType(DataType.PhoneNumber),]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Message { get; set; }

    }
}
