﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Medical.ViewModels
{
    public class CrudModel
    {
        public string TableName { get; set; }
        public string ModelName { get; set; }
        public string ContextName { get; set; }
        public string ContextDirectory { get; set; }
        public string ModelDirectory { get; set; }
    }
}
