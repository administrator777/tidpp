﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalData.Models;

namespace Medical.ViewModels
{
    public class DoctorViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public string Information { get; set; }
        public IEnumerable<WorkTime> WorkTime { get; set; }

        public string UserName { get; set; }
    }
}
