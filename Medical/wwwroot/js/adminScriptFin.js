﻿var monthselect = 1;
var yearselect = 2018;
var selectedDay = 0;
var selectedMonth = 0;
var selectedYear = 2018;
var day_no = 0;
var data = null;
var selectedInterval = 20;

var requestData = null;
var currentReceivedData = null;

window.onload = function () {
    var d = new Date();
    var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var month = d.getMonth();   //0-11
    var year = d.getFullYear(); //2014
    monthselect = month;
    yearselect = year;
    var first_date = month_name[month] + " " + 1 + " " + year;
    //September 1 2014
    var tmp = new Date(first_date).toDateString();
    //Mon Sep 01 2014 ...
    var first_day = tmp.substring(0, 3);    //Mon
    var day_name = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    day_no = day_name.indexOf(first_day);   //1
    var days = new Date(year, month + 1, 0).getDate();    //30
    //Tue Sep 30 2014 ...
    var calendar = get_calendar(days);
    document.getElementById("calendar-month-year").innerHTML = month_name[month];
    document.getElementById("calendar-year").innerHTML = year;
    calendar.className = "picker__table";
    calendar.id = "input_01_table";
    document.getElementById("calendar-dates").appendChild(calendar);
}
function init_calendar() {
    if (monthselect >= 11) {
        yearselect++;
        monthselect = -1;
    }
    var myNode = document.getElementById("calendar-dates");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    monthselect++;
    var d = new Date();
    var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    month = monthselect; //0-11
    var year = yearselect; //2018
    var first_date = month_name[month] + " " + 1 + " " + year;

    //September 1 2014
    var tmp = new Date(first_date).toDateString();
    //Mon Sep 01 2014 ...
    var first_day = tmp.substring(0, 3);    //Mon
    // var day_name = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    var day_name = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    day_no = day_name.indexOf(first_day);   //1
    var days = new Date(year, month + 1, 0).getDate();    //30

    //Tue Sep 30 2014 ...
    var calendar = get_calendar(days);
    document.getElementById("calendar-month-year").innerHTML = month_name[month];
    document.getElementById("calendar-year").innerHTML = year;
    document.getElementById("calendar-dates").appendChild(calendar);

}

function get_calendar(days) {
    var table = document.createElement('table');
    table.className = "picker__table";
    table.id = "input_01_table";
    var thead = document.createElement('thead');

    var tr = document.createElement('tr');
    var day_name = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

    //row for the day letters need to modify
    for (var c = 0; c <= 6; c++) {
        var td = document.createElement('th');
        td.className = "picker__weekday";
        td.scope = "col";
        var letter = day_name[c];
        td.innerHTML = letter;
        tr.appendChild(td);
    }
    thead.appendChild(tr);
    table.appendChild(thead);

    var daysNumber = 0;
    //create 2nd row
    tr = document.createElement('tr');
    var c;
    for (c = 0; c <= 6; c++) {
        if (c == day_no) {
            break;
        }
        daysNumber++;
        var td = document.createElement('td');
        td.innerHTML = "";
        td.id = "noday";
        td.className = "picker__day picker__day--outfocus";
        tr.appendChild(td);
    }

    var count = 1;
    for (; c <= 6; c++) {
        var tdTemp = document.createElement('td');
        var div = document.createElement('div');
        div.id = "day" + count;
        div.id = "day" + count;
        div.className = "picker__day picker__day--outfocus";
        div.innerHTML = count;
        count++;
        tdTemp.appendChild(div);
        tr.appendChild(tdTemp);
    }
    table.appendChild(tr);
    var todayDate = new Date();
    //rest of the date rows
    for (var r = 3; r <= 7; r++) {
        tr = document.createElement('tr');
        for (var c = 0; c <= 6; c++) {
            if (count > days) {
                table.appendChild(tr);
                return table;
            }
            var tdTemp = document.createElement('td');
            var div = document.createElement('div');
            div.id = "day" + count;
            div.className = "picker__day picker__day--outfocus";
            div.innerHTML = count;
            if (todayDate.getDate() == count
                && todayDate.getFullYear() == yearselect
                && todayDate.getMonth() == monthselect) {
                div.className = div.className + " picker__day--today";
            }
            count++;
            tdTemp.appendChild(div);
            tr.appendChild(tdTemp);
        }
        table.appendChild(tr);
    }
    return table;
}


function myFunction(var1) {
    var buttonValue = var1.id;
    selectedDay = buttonValue.replace('day', '');
    selectedMonth = monthselect;
    selectedYear = yearselect;
    monthselect--;
    init_calendar();
    eliminateNonAvailableDay();
    ShowDate(data, selectedDay);
    var ret = "data-123".replace('data-', '');
    var currentButton = document.getElementById(var1.id);
    document.getElementById(var1.id).className = "picker__day picker__day--infocus picker__day--selected picker__day--highlighted";
}


function addDataInInput(var1) {
    var inputBox = document.getElementById("dayBox");
    var day2 = "";
    var month2 = "";
    if (selectedDay.toString().length == 1) {
        day2 += "0";
    }
    if (selectedMonth.toString().length == 1) {
        month2 += "0";
    }
    day2 += selectedDay;
    month2 += selectedMonth + 1;
    inputBox.value = day2 + "." + month2 + "." + selectedYear + " " + var1.innerHTML;
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}


function nextMonth() {
    init_calendar();
    eliminateNonAvailableDay(data);
}
function previousMonth() {
    var todayDate = new Date();
    if (monthselect === todayDate.getMonth() && yearselect === todayDate.getFullYear()) {
        return null;
    }
    if (monthselect == 0) {
        monthselect = 10;
        yearselect = yearselect - 1;
    } else {
        monthselect -= 2;
    }
    init_calendar();
    eliminateNonAvailableDay(data);
}



function eliminateNonAvailableDay() {
    var doctorWorkTime = requestData.doctorWorkTime;
    var clientOrderTime = requestData.clientOrderTime;

    var doctor = document.getElementById("doctorName");
    // var doctorSelect = doctor.options[doctor.selectedIndex].text;
    var doctorSelect = doctor.value;


    var workingDays = new Array();
    for (var i = 0; i < doctorWorkTime.length; i++) {
        workingDays.push(doctorWorkTime[i].dayNumber);
    }

    var currentDay = 0;
    var countDay = 0;
    for (var i = 0; i < day_no; i++) {
        if (currentDay >= 7) currentDay = 0;
        currentDay++;
        countDay++;
    }

    var today = new Date();
    for (var i = 1; i <= 31; i++) {
        if (currentDay >= 7) currentDay = 0;
        if (workingDays.indexOf(currentDay) !== -1) {
            var button = document.getElementById("day" + i);
            if (button) {
                if (today.getDate() >= i && monthselect <= today.getMonth() && yearselect <= today.getFullYear()) {
                    currentDay++;
                    continue;
                }
                document.getElementById("day" + i).className = "picker__day picker__day--infocus";
                document.getElementById("day" + i).onclick = function () { myFunction(this) };
            }
        }
        currentDay++;
    }
}

function ShowDate(data, selectDate) {

    var doctorWorkTime = requestData.doctorWorkTime;
    var clientOrderTime = requestData.clientOrderTime;
    var durationProcedure = requestData.procedureDuration;
    // DELETE ALL CHILD NODE 
    var myNode = document.getElementById("addTimer");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    document.getElementById("addTimer").appendChild(document.createElement("hr"));
    // end delete all child node

    var procedureName = document.getElementById("procedureNameSelect").value;
    var doctorName = document.getElementById("doctorName").value;
    var selectedDateDay = 0;
    for (var i = 0; i < day_no; i++) {
        selectedDateDay++;
    }
    for (var i = 0; i < selectDate; i++) {
        if (selectedDateDay >= 7) selectedDateDay = 0;
        selectedDateDay++;
    }

    var doctorStartWork = "";
    var doctorEndWork = "";
    for (var i = 0; i < doctorWorkTime.length; i++) {
        if (doctorWorkTime[i].dayNumber === selectedDateDay - 1) {
            doctorStartWork += doctorWorkTime[i].startWorkingDay;
            doctorEndWork += doctorWorkTime[i].endWorkingTime;
            break;
        }
    }

    var durationTime = durationProcedure.split(":");
    var setHour = durationTime[0];
    var setMinutes = durationTime[1];
    var minuteInterval = parseInt(setHour) * 60 + parseInt(setMinutes);
    var doctorStartWorkTemp = doctorStartWork.split(":");
    var doctorEndWorkTemp = doctorEndWork.split(":");
    var currentTime = parseInt(doctorStartWorkTemp[0]) * 60 + parseInt(doctorStartWorkTemp[1]);
    var endTime = parseInt(doctorEndWorkTemp[0]) * 60 + parseInt(doctorEndWorkTemp[1]);

    // Array For Check
    var workingTime = [currentTime];
    while (currentTime + 5 <= endTime) {
        workingTime.push(currentTime + 5);
        currentTime = currentTime + 5;
    }

    //Delete Data from Array (from startClient to endClient)
    for (var i = 0; i < clientOrderTime.length; i++) {
        var value = clientOrderTime[i].dateTimeSince;
        value = value.split(/[-T:]/);
        if (parseInt(value[0]) == selectedYear && parseInt(value[1]) == (selectedMonth + 1) && selectDate == parseInt(value[2])) {
            var numhour = parseInt(value[3]);
            var nummin = parseInt(value[4]);
            var datatemp = numhour * 60 + nummin;
            var startProcedureMin = datatemp;
            var endProcedure = clientOrderTime[i].durationProcedure.split(":");
            //if appear bug in day and hour change endProcedureMin ParseInt[++]
            var endProcedureMin = parseInt(endProcedure[0]) * 60 + parseInt(endProcedure[1]);
            var endProcedureTime = startProcedureMin + endProcedureMin;
            for (var j = 0; j < workingTime.length; j++) {
                //delete before clientSince
                if (workingTime[j] > startProcedureMin - minuteInterval) {
                    for (var z = j; workingTime[z] < startProcedureMin;) {
                        workingTime.splice(z, 1);
                    }
                }
                //delete middle clientSince
                if (workingTime[j] >= startProcedureMin && workingTime[j] <= endProcedureTime) {
                    for (var z = j; workingTime[z] < endProcedureTime && z < workingTime.length;) {
                        workingTime.splice(z, 1);
                    }
                }

                if (workingTime[j] == datatemp) {
                    workingTime.splice(j, 1);
                }
            }
        }
    }

    //delete before endworking day
    for (var i = workingTime.length - 1; workingTime[i] > endTime - minuteInterval; i--) {
        workingTime.splice(i, 1);
    }


    //delete selectedInterval
    for (var i = 0; i < workingTime.length; i++) {
        var currentTime = workingTime[i];
        if (workingTime[i] + selectedInterval > workingTime[i + 1]) {
            i++;
            for (var j = i; workingTime[j] < parseInt(currentTime) + selectedInterval;) {
                workingTime.splice(j, 1);
            }
            i = i - 2;
        }
    }



    var total = document.createElement('div');

    var row = document.createElement('div');
    row.className = "row";

    var row = document.createElement('div');
    var col = document.createElement('div');
    row.className = "row";
    var count = 0;
    for (var i = 0; i < workingTime.length; i++) {
        if (i % 3 == 0) {
            row = document.createElement('div');
            row.className = "row";
            total.appendChild(row);
        }
        var col = document.createElement('div');
        col.className = "picker__day picker__day--infocus col-sm";
        col.onclick = function () { addDataInInput(this) };

        col.innerHTML = Math.floor(workingTime[i] / 60) + ":";
        var minuteInt = parseInt((workingTime[i] % 60));
        // if (minuteInt.length == 1) {
        ;
        if (minuteInt.toString().length == 1) {
            col.innerHTML += "0";
        }
        col.innerHTML += minuteInt;



        row.appendChild(col);
        count++;
        if (count >= 3) count = 0;
    }
    if (count != 0) {
        for (var i = 0; i < 3 - count; i++) {
            var col = document.createElement('div');
            col.className = "picker__day col-sm";
            row.appendChild(col);
        }
    }
    document.getElementById("addTimer").appendChild(total);

    return total;
}

function setRequestData(data) {
    requestData = data;
}

function setInterval(interval) {
    selectedInterval = interval;
}


//function for doctorModel
function setDataToDoctorModel(datatemp) {
    var temp = document.createElement('div');
    var firstName = document.createElement("h3");
    temp.appendChild(firstName);
    firstName.innerHTML = "Firstname:" + data.firstName;
    document.getElementById("doctorFirstName").appendChild(temp);
}