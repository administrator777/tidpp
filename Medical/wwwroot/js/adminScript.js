﻿

function searchFilter() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        var columnlength = tr[i].getElementsByTagName("td").length;
        for (j = 0; j < columnlength; j++) {
            td = tr[i].getElementsByTagName("td")[j];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                    break;
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
}



//
//function sortTable(selectColumn) {
//    var table, rows, switching, i, j, x, y, shouldSwitch;
//    table = document.getElementById("myTable");
//    switching = true;
//    while (switching) {
//        switching = false;
//        rows = table.getElementsByTagName("tr");
//        for (i = 1; i < (rows.length - 1); i++) {
//            shouldSwitch = false;
//            x = rows[i].getElementsByTagName("td")[selectColumn];
//            y = rows[i + 1].getElementsByTagName("td")[selectColumn];
//            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
//                shouldSwitch = true;
//                break;
//            }
//
//        }
//        if (shouldSwitch) {
//            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//            switching = true;
//        }
//    }



function sortTable(n) {
    var tempX;
    var tempY;
    var dateX;
    var dateY;

    var dateColumn = [5, 6]
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("myTable");
    switching = true;
    dir = "asc";
    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("tr");
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];


            if (dir === "asc") {

                if (dateColumn.includes(n)) {
                     tempX = x.innerHTML.split(/[: .]/);
                     tempY = y.innerHTML.split(/[: .]/);
                     dateX = new Date(tempX[2], tempX[1], tempX[0], tempX[3], tempX[4], tempX[5]);
                     dateY = new Date(tempY[2], tempY[1], tempY[0], tempY[3], tempY[4], tempY[5]);
                    if (dateX > dateY) {
                        shouldSwitch = true;
                        break;
                    }
                } else {
                    if (!isNaN(x.innerHTML) && !isNaN(y.innerHTML)) {
                        if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                    else if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            } else if (dir === "desc") {

                if (dateColumn.includes(n)) {
                     tempX = x.innerHTML.split(/[: .]/);
                     tempY = y.innerHTML.split(/[: .]/);
                     dateX = new Date(tempX[2], tempX[1], tempX[0], tempX[3], tempX[4], tempX[5]);
                     dateY = new Date(tempY[2], tempY[1], tempY[0], tempY[3], tempY[4], tempY[5]);
                    if (dateX < dateY) {
                        shouldSwitch = true;
                        break;
                    }
                } else {
                    if (!isNaN(x.innerHTML) && !isNaN(y.innerHTML)) {
                        if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                            shouldSwitch = true;
                            break;
                        }
                    } else if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount === 0 && dir === "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}



//
//function sortTable(sortColumn) {
//    var tableData = document.getElementById("myTable").getElementsByTagName('tbody').item(0);
//    var rowData = tableData.getElementsByTagName('tr');
//    for (var i = 0; i < rowData.length - 1; i++) {
//        for (var j = 0; j < rowData.length - (i + 1); j++) {
//            if (Number(rowData.item(j).getElementsByTagName('td').item(sortColumn).innerHTML.replace(/[^0-9\.]+/g, "")) < Number(rowData.item(j + 1).getElementsByTagName('td').item(sortColumn).innerHTML.replace(/[^0-9\.]+/g, ""))) {
//                tableData.insertBefore(rowData.item(j + 1), rowData.item(j));
//            }
//        }
//    }
//}
//
