window.onload = function () {
    var d = new Date();
    var month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var month = d.getMonth();   //0-11
    var year = d.getFullYear(); //2014
    monthselect = month;
    yearselect = year;
    var first_date = month_name[month] + " " + 1 + " " + year;
    //September 1 2014
    var tmp = new Date(first_date).toDateString();
    //Mon Sep 01 2014 ...
    var first_day = tmp.substring(0, 3);    //Mon
    var day_name = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    day_no = day_name.indexOf(first_day);   //1
    var days = new Date(year, month + 1, 0).getDate();    //30
    //Tue Sep 30 2014 ...
    var calendar = get_calendar(days);
    document.getElementById("calendar-month-year").innerHTML = month_name[month];
    document.getElementById("calendar-year").innerHTML = year;
    calendar.className = "picker__table";
    calendar.id = "input_01_table";
    document.getElementById("calendar-dates").appendChild(calendar);
}