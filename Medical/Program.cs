﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MedicalData;
using MedicalData.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Medical
{
    public class Program
    {
        public static async Task<IdentityResult> AssignRoles(IServiceProvider services, string email, string[] roles)
        {
            var userManager = services.GetService<UserManager<ApplicationUser>>();
            var user = await userManager.FindByEmailAsync(email);
            var result = await userManager.AddToRoleAsync(user, roles[0]);

            return result;
        }


        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();
        }

        public static void Main(string[] args)
        {
            var webHost = BuildWebHost(args);

            using (var scope = webHost.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var serviceProvider = services.GetRequiredService<IServiceProvider>();
                    var context = serviceProvider.GetRequiredService<MedicalContext>();
                    context.Database.EnsureCreated();

                    DbInitializer.CreateRoles(serviceProvider); //<---Do your seeding here
                    DbInitializer.AddAdministrator(serviceProvider);
                    DbInitializer.AddAdministrator(serviceProvider);
                    DbInitializer.AddDoctors(serviceProvider);
                    DbInitializer.AddProcedures(serviceProvider);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            //
            //                        using (var scope = webHost.Services.CreateScope())
            //                        {
            //            
            //                            var services = scope.ServiceProvider;
            //                            try
            //                            {
            ////                                SeedData.InitializeAsync(services);
            //                            }
            //                            catch (Exception ex)
            //                            {
            //                                var logger = services.GetRequiredService<ILogger<Program>>();
            //                                logger.LogError(ex, "An error occurred seeding the DB.");
            //                            }
            //                        }
            webHost.Run();
        }


        public static class SeedData
        {
            public static async void InitializeAsync(IServiceProvider serviceProvider)
            {
                string[] roles = {"ADMINISTRATOR", "Client"};
                var context = serviceProvider.GetRequiredService<MedicalContext>();
                context.Database.EnsureCreated();


                var s = !context.Doctors.Any();
                if (!context.Doctors.Any())
                {
                    context.Doctors.Add(new Doctor
                    {
                        FirstName = "Michael",
                        Lastname = "Robbins",
                        BirthDay = new DateTime(1975, 10, 1),
                        Information =
                            "If you have neck or back pain, this is where you need to go! Dr. Michael Robbins is the best chiropractor in Markham. His knowledge, skills and patience is second to.",
                        UrlImage = "/doctors/doctor1.jpg",
                        WorkTime = new[]
                        {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                    });
                    context.Doctors.Add(new Doctor
                    {
                        FirstName = "David",
                        Lastname = "Warketin",
                        BirthDay = new DateTime(1990, 10, 1),
                        Information =
                            "I've been having problems with colitis.I called Dr.Warkentin at 10pm it was so bad.Not only did he pick up but was very gracious and understanding. ",
                        UrlImage = "/doctors/doctor2.jpg",
                        WorkTime = new[]
                        {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                    });
                    context.Doctors.Add(new Doctor
                    {
                        FirstName = "Bruce",
                        Lastname = "Hoffman",
                        BirthDay = new DateTime(1960, 10, 1),
                        Information =
                            "DR.HOFFMAN TREATED ME AND MY FELLOW MARINE FOR PTSD. DR. HOFFMAN TAUGHT US HOW TO CONTROL OUT THOUGHTS",
                        UrlImage = "/doctors/doctor3.jpg",
                        WorkTime = new[]
                        {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                    });
                    context.Doctors.Add(new Doctor
                    {
                        FirstName = "Tassos",
                        Lastname = "Irikinas",
                        BirthDay = new DateTime(1975, 10, 1),
                        Information =
                            "Great experience recently. Liked the personal touch with his email. Expecting-I hope- a result similar to the ones mentioned on this site. ",
                        UrlImage = "/doctors/doctor4.jpg",
                        WorkTime = new[]
                        {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                    });
                    context.Doctors.Add(new Doctor
                    {
                        FirstName = "Jose",
                        Lastname = "Rodriquez",
                        BirthDay = new DateTime(1960, 10, 1),
                        Information =
                            "Le dr Rodriguez m'a opérée il y a 3 mois pour une hernie incisionnelle. C'est un chirurgien d'une compétence et d'une humanité hors du commun",
                        UrlImage = "/doctors/doctor5.jpg",
                        WorkTime = new[]
                        {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },

                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },

                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                    });
                    context.Doctors.Add(new Doctor
                    {
                        FirstName = "Michael",
                        Lastname = "Robbins",
                        BirthDay = new DateTime(1975, 10, 1),
                        Information =
                            "If you have neck or back pain, this is where you need to go! Dr. Michael Robbins is the best chiropractor in Markham. His knowledge, skills and patience is second to.",
                        UrlImage = "/doctors/doctor1.jpg",
                        WorkTime = new[]
                        {
                            new WorkTime
                            {
                                DayNumber = 0,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 1,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 2,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 3,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 4,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 5,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            },
                            new WorkTime
                            {
                                DayNumber = 6,
                                StartWorkingDay = new TimeSpan(08, 00, 00),
                                EndWorkingTime = new TimeSpan(18, 00, 00)
                            }
                        }
                    });
                    context.SaveChanges();
                }
                var sd = !context.Procedures.Any();

                if (!context.Procedures.Any())
                {
                    context.Procedures.Add(new Procedure
                    {
                        DurationTime = new TimeSpan(0, 20, 0),
                        Name = "Endodontic",
                        Price = 200,
                        Information =
                            "Pulpotomy — the opening of the pulp chamber of the tooth to allow an infection to drain; usually a precursor to a root canal"
                    });
                    context.Procedures.Add(new Procedure
                    {
                        DurationTime = new TimeSpan(0, 30, 0),
                        Name = "Prosthodontics",
                        Price = 200,
                        Information =
                            "Crown (caps) — artificial covering of a tooth made from a variety of biocompatible materials, including CMC/PMC (ceramic/porcelain metal composite), gold or a tin/aluminum mixture. The underlying tooth must be reshaped to accommodate these fixed restorations"
                    });
                    context.Procedures.Add(new Procedure
                    {
                        DurationTime = new TimeSpan(0, 20, 0),
                        Name = "Orthodontic ",
                        Price = 200,
                        Information =
                            "Implants and implant-supported prosthesis — also an orthodontic treatment as it involves bones"
                    });
                    context.Procedures.Add(new Procedure
                    {
                        DurationTime = new TimeSpan(0, 20, 0),
                        Name = "Periodontics ",
                        Price = 200,
                        Information = "a procedure to sever the fibers around a tooth, preventing it from relapsing"
                    });
                    context.SaveChanges();
                }



                foreach (var role in roles)
                {
                    var roleStore = new RoleStore<IdentityRole>(context);

                    if (!context.Roles.Any(r => r.Name == role))
                        await roleStore.CreateAsync(new IdentityRole(role));
                }
                if (!context.Users.Any())
                {
                    var email = "medical@gmail.com";
                    var user = new ApplicationUser
                    {
                        Email = email,
                        NormalizedEmail = email.ToUpper(),
                        UserName = email,
                        NormalizedUserName = email.ToUpper(),
                        PhoneNumber = "+76781882",
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = true,
                        SecurityStamp = Guid.NewGuid().ToString("D")
                    };

                    if (!context.Users.Any(u => u.UserName == user.UserName))
                    {
                        var password = new PasswordHasher<ApplicationUser>();
                        var hashed = password.HashPassword(user, "Asdfgh123#");
                        user.PasswordHash = hashed;
                        var userStore = new UserStore<ApplicationUser>(context);
                        var result = await userStore.CreateAsync(user);
                        await userStore.AddToRoleAsync(user, "ADMINISTRATOR");
                        var st = 0;
                        //                        if (result.Succeeded)  context.SaveChanges();
                    }
                    //
                    //                    var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
                    //                    await userManager.AddToRoleAsync(user, "Administrator");


                    //                    await AssignRoles(serviceProvider, user.Email, roles);
                }
            }
        }
    }
}