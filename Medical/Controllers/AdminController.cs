﻿using System.Linq;
using System.Threading.Tasks;
using Medical.Models.BookingViewModels;
using Medical.ViewModels;
using MedicalData;
using MedicalData.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Medical.Controllers
{
    [Authorize(Roles = "Administrator")]
    //    [Route("[controller]/[action]")]
    public class AdminController : Controller
    {
        private readonly IClient _client;
        private readonly IDoctor _doctor;

        public AdminController(IClient client, IDoctor doctor)
        {
            _client = client;
            _doctor = doctor;
        }

        public async Task<ActionResult> Doctors()
        {
            var doctors = await _doctor.GetAllDoctorsAsync();
            var model = doctors.Select(result => new DoctorViewModel
            {
                Id = result.Id,
                FirstName = result.FirstName,
                LastName = result.Lastname,
                BirthDay = result.BirthDay,
                Information = result.Information,
                WorkTime = result.WorkTime,
                UserName = result.User != null ? result.User.UserName : ""
            });
            return View(model);
        }

        [Route("[controller]/doctor/[action]/{id}")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doctor = await _doctor.GetDoctorAsync((int)id);
            if (doctor == null)
            {
                return NotFound();
            }
            var model = new DoctorViewModel
            {
                Id = doctor.Id,
                FirstName = doctor.FirstName,
                LastName = doctor.Lastname,
                BirthDay = doctor.BirthDay,
                Information = doctor.Information,
                UserName = doctor.User != null ? doctor.User.UserName : ""
            };
            return View(model);
        }


        // GET: Admin
        public ActionResult Index()
        {
            var assetModels = _client.GetAll();
            var count = 1;
            var listingResult = assetModels
                .Select(async result => new ClientIndexListingModel
                {
                    Id = count++,
                    DoctorName = await _doctor.GetDoctorNameAsync(result.Doctor.Id),
                    Procedure = result.Procedure.Name,
                    DateTime = result.DateTime,
                    Email = result.Email,
                    Comment = result.Comment,
                    RecordTime = result.RecordTime,
                    Telephone = result.Telephone,
                    FirstName = result.FirstName,
                    LastName = result.LastName
                });
            var model = new ClientListingModel
            {
                Clients = listingResult
            };

            return View(model);
        }

        // GET: Doctors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doctor = await _doctor.GetDoctorAsync((int)id);
            if (doctor == null)
            {
                return NotFound();
            }
            return View(doctor);
        }

        // POST: Doctors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,Lastname,UrlImage,BirthDay,Information")] Doctor doctor)
        {
            if (id != doctor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _doctor.UpdateAsync(doctor);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _doctor.DoctorExistAsync(doctor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(doctor);
        }

        // GET: Doctors/Create
        public IActionResult Create()
        {
            return View();
        }



        // POST: Doctors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,Lastname,UrlImage,BirthDay,Information")] Doctor doctor)
        {

            if (ModelState.IsValid)
            {
                _doctor.AddAsync(doctor);
                return RedirectToAction(nameof(Index));
            }
            return View(doctor);
        }

        // GET: Doctors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var doctor = _doctor.GetDoctor((int)id);
            if (doctor == null)
            {
                return NotFound();
            }

            return View(doctor);
        }

        // POST: Doctors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _doctor.DeleteDoctor(id);
            return RedirectToAction(nameof(Index));
        }




    }
}