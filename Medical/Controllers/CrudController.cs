﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Medical.Controllers
{
    public class CrudController : Controller
    {
        
        DataTable _dtColumns = new DataTable();
        private string _stringPrototype;



        public IActionResult Index()
        {
            CreateModel("Procedures", "Procedure", "MedicalContext", "MedicalData");
            return View();
        }

        public IActionResult CreateController()
        {
            return View();
        }

        private void CreateModel(string tableName, string modelName, string contextName, string contextDirectory)
        {
            _stringPrototype= StringsPrototype.ControllerString;

            if (tableName.Contains(" "))
            {
                tableName = tableName.Replace(" ", "");
            }
            if (modelName.Contains(" "))
            {
                modelName = modelName.Replace(" ","");
            }

            var controllerName = modelName + "Controller";
            _dtColumns = new DataTable();

            //Delete dbo. if exist or something like that
            if (tableName.Contains("."))
            {
                var currentTableName = tableName.Split(".");
                tableName = currentTableName[1];
            }
            //Pull Data from DataBase
            var idName = PullDataForController(tableName);

            //BindData from DataBase  e.g. Id, Name, Surname...
            var bindData = "";
            foreach (DataRow row in _dtColumns.Rows)
            {
                bindData += row["COLUMN_NAME"];
                if (_dtColumns.Rows.IndexOf(row) != _dtColumns.Rows.Count - 1)
                    bindData += ",";
            }

            //get current DirectoryProject
            var directoryProject =
                AppContext.BaseDirectory.Substring(0,
                    AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));
            var directoryModels = directoryProject + "/Controllers";
            //Get current Project Name for Saving namespaces
            var currentProjectName = directoryProject.Split("\\").GetValue(directoryProject.Split("\\").Length - 2)
                .ToString();

            // If File exist delete it before create
            if (System.IO.File.Exists(directoryModels + "/" + modelName + "Controller" + ".cs"))
                System.IO.File.Delete(directoryModels + "/" + modelName + "Controller" + ".cs");

            //Replace Data in New controller
            ReplaceDataInController(tableName, controllerName, bindData, contextName, contextDirectory, modelName,
                currentProjectName, idName);

            //Create new Controller File
            using (var file =
                new StreamWriter(directoryModels + "/" + modelName + "Controller" + ".cs", true))
            {
                file.WriteLine(_stringPrototype);
            }
            // System.IO.File.Create(directoryModels + "/" + tableName + ".cs");
        }




        private void ReplaceDataInController(string tableName, string controllerName, string bindData,
            string contextName, string contextDirectory, string modelName, string namespaceName, string idName)
        {
            _stringPrototype = _stringPrototype.Replace("<context_table>", tableName);
            _stringPrototype = _stringPrototype.Replace("<controller_name>", controllerName);
            _stringPrototype = _stringPrototype.Replace("<bind_data>", bindData);
            _stringPrototype = _stringPrototype.Replace("<context_name>", contextName);
            _stringPrototype = _stringPrototype.Replace("<context_directory>", contextDirectory);
            _stringPrototype = _stringPrototype.Replace("<context_modelName>", modelName);
            _stringPrototype = _stringPrototype.Replace("<namespace>", namespaceName + ".Controllers");
            _stringPrototype = _stringPrototype.Replace("<id>", idName);
        }


        private string PullDataForController(string tableName)
        {
            string idName;
            var queryColumnsName = @"select * from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "'";
            var querryId = @"SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_NAME='PK_" + tableName + "'";
            var conn = new SqlConnection(
                @"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=MedicalDataBase;");
            var cmd = new SqlCommand(queryColumnsName, conn);
            var cmdId = new SqlCommand(querryId, conn);
            conn.Open();

            // create data adapter
            var da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(_dtColumns);

            //Connection for Receive name of ID from DataBase
            da = new SqlDataAdapter(cmdId);
            DataTable idTable = new DataTable();
            da.Fill(idTable);
            idName = ReadIdFromTable(idTable);

            // Close SQl Connection
            conn.Close();
            da.Dispose();
            return idName;
        }


        //Read Key Id from DataTable and return string id anem
        private string ReadIdFromTable(DataTable idTable)
        {
            var idName = "";
            foreach (DataRow row in idTable.Rows)
            {
                idName += row["COLUMN_NAME"];
                break;
            }
            return idName;
        }


        private string ReadIdName(string bindData)
        {
            var currentNames = bindData.Split(",");
            var possibleVariant = "id";
            foreach (var name in currentNames)
            {
                if (possibleVariant.Contains(name.ToUpper()))
                {
                    return name;
                }
            }
            return "";
        }


    }
}