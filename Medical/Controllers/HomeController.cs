﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Medical.Models;
using Medical.Models.BookingViewModels;
using Medical.Models.HomeViewModels;
using MedicalData;
using MedicalData.Models;
using Microsoft.AspNetCore.Mvc;

namespace Medical.Controllers
{
    public class HomeController : Controller
    {
        private readonly IClient _client;
        private readonly IDoctor _doctors;
        private readonly IProcedure _procedures;

        public HomeController(IClient client, IProcedure procedures, IDoctor doctors)
        {
            _client = client;
            _procedures = procedures;
            _doctors = doctors;
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }


        [HttpGet]
        public async Task<ActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ClientOrderModel model)
        {
            if (ModelState.IsValid)
            {
                var names = model.FullName.Split(' ');
                var firstName = names[0];
                var lastName = names[1];

                _client.Add(new Client
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Procedure = await _procedures.GetProcedureAsync(model.ProcedureName),
                    Email = model.Email,
                    DateTime = model.DateTimeSince,
                    RecordTime = DateTime.Now,
                    Doctor = await _doctors.GetDoctorAsync(model.DoctorName),
                    Comment = model.Message,
                    Telephone = model.PhoneNumber
                });
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }


        [HttpPost]
        public async Task<JsonResult> GetClientDoctorAndProcedureDuration(DoctorAndProcedureNames std)
        {
            var doctorId = await _doctors.GetDoctorIdAtFullNameAsync(std.DoctorFullName);
            var clientDateTimeSince = await _doctors.GetAllClientTimeFromDoctorAsync(doctorId);
            var doctorWorkTime = await _doctors.GetDoctorWorkingTimeAsync(doctorId);

            var model = new ClientSinceTimeAndDoctorWorkTimeModel
            {
                ClientOrderTime = clientDateTimeSince,
                DoctorWorkTime = doctorWorkTime,
                ProcedureDuration = await _procedures.GetDurationProcedureAsync(std.ProcedureFullName)
            };
            return Json(model);
        }


        [HttpPost]
        public async Task<JsonResult> GetDoctorData(string fullName)
        {
            var model = await _client.GetDoctorAsync(fullName);
            return Json(model);
        }

        [HttpPost]
        public async Task<JsonResult> GetDoctorsAndProcedures()
        {
            var assetModelDoctor = await _doctors.GetAllDoctorsAsync();
            var assetModelProcedure = await _procedures.GetAllProceduresAsync();

            var doctorsNames = assetModelDoctor
                .Select(async result => new DoctorName
                {
                    FullName = await _doctors.GetDoctorNameAsync(result.Id)
                });

            var procedureNames = assetModelProcedure
                .Select(result => new ProcedureName
                {
                    Name = result.Name
                });

            var doctorAndProceduresNames = new DoctorAndNamesViewModel
            {
                DoctorsNames = doctorsNames,
                ProceduresName = procedureNames
            };
            return Json(doctorAndProceduresNames);
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}