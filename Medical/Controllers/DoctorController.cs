﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Medical.Extensions;
using MedicalData;
using MedicalData.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Medical.Controllers
{
    [Authorize(Roles = "Doctor")]
    public class DoctorController : Controller
    {
        private readonly IClient _client;
        private readonly IDoctor _doctor;
        private readonly MedicalContext _context;
        public DoctorController(IClient client, IDoctor doctor, MedicalContext context)
        {
            _client = client;
            _doctor = doctor;
            _context = context;
        }

        public IActionResult Index()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == userId);
            ViewBag.UserData = currentUser;
            ViewBag.UserId = userId;
            return View();
        }

        public async Task<IActionResult> GetClientListAsync(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var clients = _client.GettAllQuerable();

            if (!String.IsNullOrEmpty(searchString))
            {
                clients = clients.Where(s => s.FirstName.Contains(searchString)
                                               || s.LastName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    clients = clients.OrderByDescending(s => s.LastName);
                    break;
                case "Date":
                    clients = clients.OrderBy(s => s.DateTime);
                    break;
                case "date_desc":
                    clients = clients.OrderByDescending(s => s.DateTime);
                    break;
                default:
                    clients = clients.OrderBy(s => s.FirstName);
                    break;
            }
            int pageSize = 3;
            return View(await PaginatedList<Client>.CreateAsync(clients.AsNoTracking(), page ?? 1, pageSize));
        }



        public IActionResult Printdata()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            ApplicationUser currentUser = _context.Users.FirstOrDefault(x => x.Id == userId);
            return View(currentUser);
        }
    }
}