using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MedicalData;
using MedicalData.Models;


namespace Medical.Controllers
{
     [Route("[controller]/[action]")]
    public class ProcedureController : Controller
    {
        private readonly MedicalContext _context;
        
        public ProcedureController(MedicalContext context)
        {
            _context = context;
        }
        
        //GET: Procedures
        public async Task<IActionResult> Index()
        {
            return View(await _context.Procedures.ToListAsync());
        }
        
        // GET: Procedures/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            System.Diagnostics.Debugger.Break();
            return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return NotFound();
            }

            var data = await _context.Procedures
                .SingleOrDefaultAsync(m => m.Id == id);
            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }

        // GET: Procedures/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Procedures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DurationTime,Information,Name,Price")] Procedure data)
        {
            if (ModelState.IsValid)
            {
                _context.Add(data);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(data);
        }


        // GET: Procedures/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var data = await _context.Procedures.SingleOrDefaultAsync(m => m.Id == id);
            if (data == null)
            {
                return NotFound();
            }
            return View(data);
        }

        // POST: Procedures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DurationTime,Information,Name,Price")] Procedure  data)
        {
            if (id != data.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(data);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientExists(data.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(data);
        }


        // GET: Procedures/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var data = await _context.Procedures
                .SingleOrDefaultAsync(m => m.Id == id);
            if (data == null)
            {
                return NotFound();
            }

            return View(data);
        }


        // POST: Procedures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var data = await _context.Procedures.SingleOrDefaultAsync(m => m.Id == id);
            _context.Procedures.Remove(data);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

         private bool ClientExists(int id)
        {
            return _context.Procedures.Any(e => e.Id == id);
        }
    }
}
