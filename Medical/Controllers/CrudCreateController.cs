﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Loader;
using Medical.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CSharp;

namespace Medical.Controllers
{
    public class CrudCreateController : Controller
    {
        //For Controller
        private DataTable _dtColumns = new DataTable();
        private string _stringPrototype;
        //end controller

        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Index(CrudModel model)
        {



            if (ModelState.IsValid)
            {
                var tableName = model.TableName;
                var modelName = model.ModelName; // Name from DBSet<>
                var modelDirectory = model.ModelDirectory;
                var contextName = model.ContextName;
                var contextDirectory = model.ContextDirectory;



                //                var controllerName = "ProcedureController";
                //                var modelsName = "Procedures"; // Name from DBSet<>
                //                var modelDirectory = "MedicalData.Models.";
                //                CreateView(controllerName, modelsName, modelDirectory, "Create");

                CreateView(modelName, tableName, modelDirectory, "Create");
                CreateView(modelName, tableName, modelDirectory, "Delete");
                CreateView(modelName, tableName, modelDirectory, "Details");
                CreateView(modelName, tableName, modelDirectory, "Edit");
                CreateIndexView(modelName, tableName, modelDirectory, "Index");

                CreateController(tableName, modelName, contextName, contextDirectory);





                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }


        //Test COde




        //end test code




        private DataTable PullData(DataTable dataTable, string tableName)
        {
            var queryColumnsName = @"select * from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "'";
            var conn = new SqlConnection(
                @"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=MedicalDataBase;");
            var cmd = new SqlCommand(queryColumnsName, conn);
            conn.Open();
            // create data adapter
            var da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(dataTable);
            // Close SQl Connection
            conn.Close();
            da.Dispose();
            return dataTable;
        }
        #region ViewCreate

        #region ViewCreateDeleteDetailsEdit

        private void CreateView(string controllerName, string tableName, string modelDirectory,
            string viewName = "Create")
        {
            //DataTable for Receiving Data from 
            var dtColumns = new DataTable();
            //Pull Data from DataBase
            dtColumns = PullData(dtColumns, tableName);

            if (controllerName.Contains("Controller"))
                controllerName = controllerName.Replace("Controller", "");

            //get current DirectoryProject
            var directoryProject =
                AppContext.BaseDirectory.Substring(0,
                    AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));

            var viewDirectory = directoryProject + "/Views/" + controllerName;
            //            var viewDirectory = directoryProject + "/Views/" + tableName;
            if (!Directory.Exists(viewDirectory))
                Directory.CreateDirectory(viewDirectory);


            // If File exist delete it before create
            if (System.IO.File.Exists(viewDirectory + "/" + viewName + ".cshtml"))
                System.IO.File.Delete(viewDirectory + "/" + viewName + ".cshtml");



            //Replace Data in New controller
            var modelDirectoryInjection = modelDirectory + controllerName;

            var stringView = "";
            var viewPrototype = "";
            switch (viewName)
            {
                case "Create":
                    stringView = StringsPrototype.CreateView;
                    viewPrototype = StringsPrototype.CreateViewPrototype;
                    break;
                case "Delete":
                    stringView = StringsPrototype.DeleteView;
                    viewPrototype = StringsPrototype.DeleteViewPrototype;
                    break;
                case "Details":
                    stringView = StringsPrototype.DetailsView;
                    viewPrototype = StringsPrototype.DetailsViewPrototype;
                    break;
                case "Edit":
                    stringView = StringsPrototype.EditView;
                    viewPrototype = StringsPrototype.EditViewPrototype;
                    break;
                default:
                    return;
            }

            var viewCreateData = ReplaceDataInView(modelDirectoryInjection, dtColumns, stringView, viewPrototype);

            //Create new Controller File
            using (var file =
                new StreamWriter(viewDirectory + "/" + viewName + ".cshtml", true))
            {
                file.WriteLine(viewCreateData);
            }
            //            string result = Razor.Parse(viewCreateData);
            //            var t = 0;
            // System.IO.File.Create(directoryModels + "/" + tableName + ".cs");
        }

        private string ReplaceDataInView(string modelDirectory, DataTable dataTable, string stringView,
            string viewPrototype, string layoutDirectory = "~/Views/Shared/_Layout.cshtml")
        {
            var viewCreateData = stringView;
            viewCreateData = viewCreateData.Replace("<model_data>", modelDirectory);
            viewCreateData = viewCreateData.Replace("<layout_directory>", layoutDirectory);
            //BindData from DataBase  e.g.  Name, Surname...
            var bindData = "";
            //Id name 
            var idName = "";
            foreach (DataRow row in dataTable.Rows)
            {
                var columnName = row["COLUMN_NAME"].ToString();

                // Check if current row data is ID =>continue to next row
                if (columnName.ToLower() == "id")
                {
                    idName = columnName;
                    continue;
                }

                var tempData = viewPrototype;
                tempData = tempData.Replace("<model_row>", columnName);
                bindData += tempData + "\n";
            }
            viewCreateData = viewCreateData.Replace("<injection_data>", bindData);
            if (viewCreateData.Contains("<id>"))
                viewCreateData = viewCreateData.Replace("<id>", idName);
            return viewCreateData;
        }

        #endregion


        #region ViewIndex

        private void CreateIndexView(string controllerName, string tableName, string modelDirectory,
            string viewName = "Index")
        {
            //DataTable for Receiving Data from 
            var dtColumns = new DataTable();
            //Pull Data from DataBase
            dtColumns = PullData(dtColumns, tableName);

            if (controllerName.Contains("Controller"))
                controllerName = controllerName.Replace("Controller", "");

            //get current DirectoryProject
            var directoryProject =
                AppContext.BaseDirectory.Substring(0,
                    AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));

            var viewDirectory = directoryProject + "/Views/" + controllerName;
            if (!Directory.Exists(viewDirectory))
                Directory.CreateDirectory(viewDirectory);

            // If File exist delete it before create
            if (System.IO.File.Exists(viewDirectory + "/" + viewName + ".cshtml"))
                System.IO.File.Delete(viewDirectory + "/" + viewName + ".cshtml");

            //Replace Data in New controller
            var modelDirectoryInjection = modelDirectory + controllerName;

            var stringView = StringsPrototype.IndexView;
            var viewIndexPrototypeHead = StringsPrototype.IndexViewPrototypeHead;
            var viewIndexPrototypeBody = StringsPrototype.IndexViewPrototypeBody;

            var viewCreateData = ReplaceDataInIndexView(modelDirectoryInjection, dtColumns, stringView,
                viewIndexPrototypeHead, viewIndexPrototypeBody);

            //Create new Controller File
            using (var file =
                new StreamWriter(viewDirectory + "/" + viewName + ".cshtml", true))
            {
                file.WriteLine(viewCreateData);
            }
            // System.IO.File.Create(directoryModels + "/" + tableName + ".cs");
        }

        private string ReplaceDataInIndexView(string modelDirectory, DataTable dataTable, string stringView,
            string viewPrototypeHead, string viewPrototypeBody,
            string layoutDirectory = "~/Views/Shared/_Layout.cshtml")
        {
            var viewCreateData = stringView;
            viewCreateData = viewCreateData.Replace("<model_data>", modelDirectory);
            viewCreateData = viewCreateData.Replace("<layout_directory>", layoutDirectory);
            //BindData from DataBase  e.g.  Name, Surname...
            var bindHeadData = "";
            var bindBodyData = "";
            var idName = "";
            foreach (DataRow row in dataTable.Rows)
            {
                var columnName = row["COLUMN_NAME"].ToString();

                // Check if current row data is ID =>continue to next row
                if (columnName.ToLower() == "id")
                {
                    idName = columnName;
                    continue;
                }

                var tempHeadData = viewPrototypeHead;
                tempHeadData = tempHeadData.Replace("<model_row>", columnName);
                bindHeadData += tempHeadData + "\n";

                var tempBodyData = viewPrototypeBody;
                tempBodyData = tempBodyData.Replace("<model_row>", columnName);
                bindBodyData += tempBodyData + "\n";
            }
            viewCreateData = viewCreateData.Replace("<injection_data>", bindHeadData);
            viewCreateData = viewCreateData.Replace("<injection_body>", bindBodyData);
            if (viewCreateData.Contains("<id>"))
                viewCreateData = viewCreateData.Replace("<id>", idName);
            return viewCreateData;
        }

        #endregion

        #endregion


        #region CreateController

        private string CreateController(string tableName, string modelName, string contextName, string contextDirectory)
        {
            _stringPrototype = StringsPrototype.ControllerString;

            if (tableName.Contains(" "))
                tableName = tableName.Replace(" ", "");
            if (modelName.Contains(" "))
                modelName = modelName.Replace(" ", "");

            var controllerName = modelName + "Controller";
            _dtColumns = new DataTable();

            //Delete dbo. if exist or something like that
            if (tableName.Contains("."))
            {
                var currentTableName = tableName.Split(".");
                tableName = currentTableName[1];
            }
            //Pull Data from DataBase
            var idName = PullDataForController(tableName);

            //BindData from DataBase  e.g. Id, Name, Surname...
            var bindData = "";
            foreach (DataRow row in _dtColumns.Rows)
            {
                bindData += row["COLUMN_NAME"];
                if (_dtColumns.Rows.IndexOf(row) != _dtColumns.Rows.Count - 1)
                    bindData += ",";
            }

            //get current DirectoryProject
            var directoryProject =
                AppContext.BaseDirectory.Substring(0,
                    AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));
            var directoryModels = directoryProject + "/Controllers";
            //Get current Project Name for Saving namespaces
            var currentProjectName = directoryProject.Split("\\").GetValue(directoryProject.Split("\\").Length - 2)
                .ToString();

            // If File exist delete it before create
            if (System.IO.File.Exists(directoryModels + "/" + modelName + "Controller" + ".cs"))
                System.IO.File.Delete(directoryModels + "/" + modelName + "Controller" + ".cs");

            //Replace Data in New controller
            ReplaceDataInController(tableName, controllerName, bindData, contextName, contextDirectory, modelName,
                currentProjectName, idName);

            //Create new Controller File
            using (var file =
                new StreamWriter(directoryModels + "/" + modelName + "Controller" + ".cs", true))
            {
                file.WriteLine(_stringPrototype);
            }
            // System.IO.File.Create(directoryModels + "/" + tableName + ".cs");
            return _stringPrototype;
        }


        private void ReplaceDataInController(string tableName, string controllerName, string bindData,
            string contextName, string contextDirectory, string modelName, string namespaceName, string idName)
        {
            _stringPrototype = _stringPrototype.Replace("<context_table>", tableName);
            _stringPrototype = _stringPrototype.Replace("<controller_name>", controllerName);
            _stringPrototype = _stringPrototype.Replace("<bind_data>", bindData);
            _stringPrototype = _stringPrototype.Replace("<context_name>", contextName);
            _stringPrototype = _stringPrototype.Replace("<context_directory>", contextDirectory);
            _stringPrototype = _stringPrototype.Replace("<context_modelName>", modelName);
            _stringPrototype = _stringPrototype.Replace("<namespace>", namespaceName + ".Controllers");
            _stringPrototype = _stringPrototype.Replace("<id>", idName);
        }


        private string PullDataForController(string tableName)
        {
            string idName;
            var queryColumnsName = @"select * from INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "'";
            var querryId = @"SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_NAME='PK_" + tableName +
                           "'";
            var conn = new SqlConnection(
                @"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=MedicalDataBase;");
            var cmd = new SqlCommand(queryColumnsName, conn);
            var cmdId = new SqlCommand(querryId, conn);
            conn.Open();

            // create data adapter
            var da = new SqlDataAdapter(cmd);
            // this will query your database and return the result to your datatable
            da.Fill(_dtColumns);

            //Connection for Receive name of ID from DataBase
            da = new SqlDataAdapter(cmdId);
            var idTable = new DataTable();
            da.Fill(idTable);
            idName = ReadIdFromTable(idTable);

            // Close SQl Connection
            conn.Close();
            da.Dispose();
            return idName;
        }


        //Read Key Id from DataTable and return string id anem
        private string ReadIdFromTable(DataTable idTable)
        {
            var idName = "";
            foreach (DataRow row in idTable.Rows)
            {
                idName += row["COLUMN_NAME"];
                break;
            }
            return idName;
        }

        #endregion


        /*
                #region CreateView
                private void CreateCreateView(string controllerName, string tableName, string modelDirectory, string viewName = "Create")
                {
                    //DataTable for Receiving Data from 
                    DataTable dtColumns = new DataTable();
                    //Pull Data from DataBase
                    dtColumns = PullData(dtColumns, tableName);

                    if (controllerName.Contains("Controller"))
                    {
                        controllerName = controllerName.Replace("Controller", "");
                    }



                    //get current DirectoryProject
                    var directoryProject =
                        AppContext.BaseDirectory.Substring(0,
                            AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));

                    var viewDirectory = directoryProject + "/Views/" + controllerName;
                    if (!Directory.Exists(viewDirectory))
                    {
                        Directory.CreateDirectory(viewDirectory);
                    }

                    // If File exist delete it before create
                    if (System.IO.File.Exists(viewDirectory + "/" + viewName + ".cshtml"))
                        System.IO.File.Delete(viewDirectory + "/" + viewName + ".cshtml");

                    //Replace Data in New controller

                    var viewCreateData = ReplaceDataInCreateView(modelDirectory + controllerName, dtColumns);

                    //Create new Controller File
                    using (var file =
                        new StreamWriter(viewDirectory + "/" + viewName + ".cshtml", true))
                    {
                        file.WriteLine(viewCreateData);
                    }
                    // System.IO.File.Create(directoryModels + "/" + tableName + ".cs");

                }

                private string ReplaceDataInCreateView(string modelDirectory, DataTable dataTable, string layoutDirectory = "~/Views/Shared/_Layout.cshtml")
                {
                    var viewCreateData = StringsPrototype.createView;
                    viewCreateData = viewCreateData.Replace("<model_data>", modelDirectory);
                    viewCreateData = viewCreateData.Replace("<layout_directory>", layoutDirectory);
                    //BindData from DataBase  e.g. Id, Name, Surname...
                    var bindData = "";
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var columnName = row["COLUMN_NAME"].ToString();

                        // Check if current row data is ID =>continue to next row
                        if (columnName.ToLower() == "id") continue;

                        var tempData = StringsPrototype.createViewPrototype;
                        tempData = tempData.Replace("<model_row>", columnName);
                        bindData += tempData + "\n";
                    }
                    viewCreateData = viewCreateData.Replace("<injection_data>", bindData);
                    return viewCreateData;
                }
                #endregion
        */



        /*     var controllerName = "ProcedureController";
             var modelsName = "Procedures"; // Name from DBSet<>
             var modelDirectory = "MedicalData.Models.";
             CreateView(controllerName, modelsName, modelDirectory, "Create");
             CreateView(controllerName, modelsName, modelDirectory, "Delete");
             CreateView(controllerName, modelsName, modelDirectory, "Details");
             CreateView(controllerName, modelsName, modelDirectory, "Edit");

             CreateIndexView(controllerName, modelsName, modelDirectory, "Index");*/
    }
}