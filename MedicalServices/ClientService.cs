﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalData;
using MedicalData.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MedicalServices
{
    public class ClientService : IClient
    {
        private readonly MedicalContext _context;

        public ClientService(MedicalContext context)
        {
            _context = context;
        }

        public IEnumerable<Client> GetAll()
        {
            return _context.Client
                .Include(asset => asset.Doctor)
                .Include(asset => asset.Procedure);
        }

        public IQueryable<Client> GettAllQuerable()
        {
            return _context.Client
                .Include(asset => asset.Doctor)
                .Include(asset => asset.Procedure);
        }

        public void Add(Client newClient)
        {
            _context.Client.Add(newClient);
            _context.SaveChanges();
        }


        public async void AddAsync(Client newClient)
        {
            await _context.AddAsync(newClient);
            await _context.SaveChangesAsync();
        }

        public void Add(Doctor newDoctor)
        {
            _context.Doctors.Add(newDoctor);
            _context.SaveChanges();
        }

        public async void AddAsync(Doctor newDoctor)
        {
            await _context.AddAsync(newDoctor);
            await _context.SaveChangesAsync();
        }

        public void Add(Procedure newProcedure)
        {
            _context.Procedures.Add(newProcedure);
            _context.SaveChanges();
        }


        public async void AddAsync(Procedure newProcedure)
        {
            await _context.AddAsync(newProcedure);
            await _context.SaveChangesAsync();
        }

        public async Task<string> GetDoctorName(int id)
        {
            if (_context.Doctors.Any(a => a.Id == id))
            {
                var currentDoctor = await _context.Doctors
                    .FirstOrDefaultAsync(a => a.Id == id);
                var firstName = currentDoctor.FirstName;
                var lastName = currentDoctor.Lastname;
                return firstName + " " + lastName;
            }
            return "";
        }


        public Doctor GetDoctor(string name)
        {
            var names = name.Split(' ');
            var firstName = names[0];
            var lastName = names[1];
            return _context.Doctors
                .FirstOrDefault(a => a.FirstName == firstName && a.Lastname == lastName);
        }

        public async Task<Doctor> GetDoctorAsync(string name)
        {
            var names = name.Split(' ');
            var firstName = names[0];
            var lastName = names[1];
            var doctor = await _context.Doctors
                .FirstOrDefaultAsync(a => a.FirstName == firstName && a.Lastname == lastName);
            return doctor;
        }

        public async Task<IEnumerable<Doctor>> GetDoctors()
        {
            return await _context.Doctors.ToListAsync();
        }

        public Procedure GetProcedure(string name)
        {
            return _context.Procedures
                .FirstOrDefault(a => a.Name == name);
        }

        public DateTime GetDateTime(int id)
        {
            if (_context.Client.Any(a => a.Id == id))
                return _context.Client
                    .FirstOrDefault(a => a.Id == id).DateTime;
            return new DateTime();
        }

        public async Task<string> GetEmailAsync(int id)
        {
            if (_context.Client.Any(a => a.Id == id))
            {
                return _context.Client
                    .FirstOrDefaultAsync(a => a.Id == id).Result.Email;
            }

            return "";
        }

        public string GetComment(int id)
        {
            if (_context.Client.Any(a => a.Id == id))
                return _context.Client
                    .FirstOrDefault(a => a.Id == id)
                    ?.Comment;
            return "";
        }


        public async Task<Procedure> GetProceduresById(int id)
        {
            return await _context.Procedures
                .FirstOrDefaultAsync(a => a.Id == id);
        }

        public IEnumerable<Procedure> GetAllProcedures()
        {
            return _context.Procedures;
        }

        public async Task<IEnumerable<Procedure>> GetAllProceduresAsync()
        {
            return await _context.Procedures.ToListAsync();
        }

        public IEnumerable<Doctor> GetAllDoctors()
        {
            return _context.Doctors
                .Include(x => x.WorkTime)
                .Include(x => x.User);
        }
        public async Task<IEnumerable<Doctor>> GetAllDoctorsAsync()
        {
            return await _context.Doctors
                .Include(asset => asset.WorkTime)
                .Include(x => x.User).ToListAsync();
        }

        public async Task<IEnumerable<WorkTime>> GetDoctorWorkingTimeAsync(int doctorId)
        {
            if (_context.Doctors.Any(x => x.Id == doctorId))
            {
                var temp = await _context.Doctors
                    .Include(x => x.WorkTime)
                    .Where(x => x.Id == doctorId).ToListAsync();

                IEnumerable<WorkTime> tempdata = new WorkTime[0];
                foreach (var doctor in temp)
                    tempdata = tempdata.Concat(doctor.WorkTime);
                return tempdata;
            }
            return null;
        }

        public async Task<IEnumerable<ClientOrderAndTime>> GetAllClientTimeFromDoctor(int id)
        {
            var currentTime = DateTime.Now;
            var clientOrderAndTime = await _context.Client
                .Include(x => x.Procedure)
                .Include(x => x.Doctor)
                .Where(x => x.DateTime >= currentTime && x.Doctor.Id == id)
                .Select(result => new ClientOrderAndTime
                {
                    DateTimeSince = result.DateTime,
                    DurationProcedure = result.Procedure.DurationTime
                }).ToListAsync();
            return clientOrderAndTime;
        }

        public async Task<string> GetDoctorsEmail(string id)
        {

            return "";
        }

        public async Task<int> GetDoctorIdAtFullName(string fullName)
        {
            var doctorId = await _context.Doctors
                .FirstOrDefaultAsync(x => x.FirstName + " " + x.Lastname == fullName);

            return doctorId.Id;
        }

        public TimeSpan GetDurationProcedure(string procedureName)
        {
            if (_context.Procedures.Any(x => x.Name == procedureName))
                return _context.Procedures
                    .FirstOrDefault(x => x.Name == procedureName).DurationTime;
            return new TimeSpan(0, 0, 0, 0);
        }
        public async Task<TimeSpan> GetDurationProcedureASync(string procedureName)
        {
            if (_context.Procedures.Any(x => x.Name == procedureName))
            {
                var durationTime = await _context.Procedures
                    .FirstOrDefaultAsync(x => x.Name == procedureName);
                return durationTime.DurationTime;

            }
            return new TimeSpan(0, 0, 0, 0);
        }


        //For seeding Data

        public async Task<IdentityResult> AddUserAsync(ApplicationUser user)
        {
            var userStore = new UserStore<ApplicationUser>(_context);
            var result = await userStore.CreateAsync(user);
            if (result.Succeeded)
            {
                //                Task<IdentityResult> newUserRole = userManager.AddToRoleAsync(administrator, "Administrator");
                //                newUserRole.Wait();
                await userStore.AddToRoleAsync(user, "ADMINISTRATOR");
                //                await _context.SaveChangesAsync();
            }
            return result;
        }

        public async Task<bool> CheckUserExistAsync(string username)
        {
            var exist = await _context.Users.AnyAsync(x => x.UserName == username);
            return exist;
        }
        public bool CheckIfUserExist()
        {
            return _context.Users.Any();
        }
    }
}