﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalData;
using MedicalData.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace MedicalServices
{
    public class DoctorService : IDoctor
    {
        private readonly MedicalContext _context;

        public DoctorService(MedicalContext context)
        {
            _context = context;
        }

        public void Add(Doctor newDoctor)
        {
            _context.Doctors.Add(newDoctor);
            _context.SaveChanges();
        }

        public async void AddAsync(Doctor newDoctor)
        {
            await _context.Doctors.AddAsync(newDoctor);
            await _context.SaveChangesAsync();
        }

        public async void UpdateAsync(Doctor doctor)
        {
            _context.Update(doctor);
            await _context.SaveChangesAsync();
        }

        public Doctor GetDoctor(string fullName)
        {
            var names = fullName.Split(' ');
            var firstName = names[0] ?? "";
            var lastName = names[1] ?? "";
            return _context.Doctors
                .FirstOrDefault(a => a.FirstName == firstName && a.Lastname == lastName);
        }

        public Doctor GetDoctor(int doctorId)
        {
            return _context.Doctors.FirstOrDefault(x => x.Id == doctorId);
        }

        public async Task<Doctor> GetDoctorAsync(string fullName)
        {
            var names = fullName.Split(' ');
            var firstName = names[0] ?? "";
            var lastName = names[1] ?? "";
            var doctor = await _context.Doctors
                .FirstOrDefaultAsync(a => a.FirstName == firstName && a.Lastname == lastName);
            return doctor;
        }

        public async Task<Doctor> GetDoctorAsync(int doctorId)
        {
            return await _context.Doctors
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == doctorId);
        }

        public IEnumerable<Doctor> GetAllDoctors()
        {
            var doctors = _context.Doctors
                .Include(x => x.User);
            return doctors;
        }

        public async Task<IEnumerable<Doctor>> GetAllDoctorsAsync()
        {
            var doctors = await _context.Doctors
                .Include(x => x.User)
                .ToListAsync();
            return doctors;
        }

        public async Task<IEnumerable<WorkTime>> GetDoctorWorkingTimeAsync(int doctorId)
        {
            if (_context.Doctors.Any(x => x.Id == doctorId))
            {
                var temp = await _context.Doctors
                    .Include(x => x.WorkTime)
                    .Where(x => x.Id == doctorId).ToListAsync();

                IEnumerable<WorkTime> tempdata = new WorkTime[0];
                foreach (var doctor in temp)
                    tempdata = tempdata.Concat(doctor.WorkTime);
                return tempdata;
            }
            return null;
        }

        public string GetDoctorName(int doctorId)
        {
            if (_context.Doctors.Any(a => a.Id == doctorId))
            {
                var currentDoctor = _context.Doctors
                    .FirstOrDefault(a => a.Id == doctorId);
                if (currentDoctor != null)
                {
                    var firstName = currentDoctor.FirstName;
                    var lastName = currentDoctor.Lastname;
                    return firstName + " " + lastName;
                }
            }
            return "";
        }

        public async Task<string> GetDoctorNameAsync(int doctorId)
        {
            if (_context.Doctors.Any(a => a.Id == doctorId))
            {
                var currentDoctor = await _context.Doctors
                    .FirstOrDefaultAsync(a => a.Id == doctorId);
                var firstName = currentDoctor.FirstName;
                var lastName = currentDoctor.Lastname;
                return firstName + " " + lastName;
            }
            return "";
        }


        public int GetDoctorIdAtFullName(string fullName)
        {
            var names = fullName.Split(' ');
            var firstName = names[0] ?? "";
            var lastName = names[1] ?? "";
            var doctor = _context.Doctors.FirstOrDefault(x => x.FirstName == firstName && x.Lastname == lastName);
            return doctor.Id;
        }

        public async Task<int> GetDoctorIdAtFullNameAsync(string fullName)
        {
            var names = fullName.Split(' ');
            var firstName = names[0] ?? "";
            var lastName = names[1] ?? "";
            var doctor =
                await _context.Doctors.FirstOrDefaultAsync(x => x.FirstName == firstName && x.Lastname == lastName);
            return doctor.Id;
        }

        public async Task<IEnumerable<ClientOrderAndTime>> GetAllClientTimeFromDoctorAsync(int doctorId)
        {
            var currentTime = DateTime.Now;
            var clientOrderAndTime = await _context.Client
                .Include(x => x.Procedure)
                .Include(x => x.Doctor)
                .Where(x => x.DateTime >= currentTime && x.Doctor.Id == doctorId)
                .Select(result => new ClientOrderAndTime
                {
                    DateTimeSince = result.DateTime,
                    DurationProcedure = result.Procedure.DurationTime
                }).ToListAsync();
            return clientOrderAndTime;
        }


        public async Task<bool> DoctorExistAsync(int doctorId)
        {
            return await _context.Doctors.AnyAsync(x => x.Id == doctorId);
        }

        public void DeleteDoctor(int doctorId)
        {
            var doctor = _context.Doctors
                .Include(x => x.User)
                .Include(x => x.WorkTime)
                .FirstOrDefault(x => x.Id == doctorId);

            if (doctor == null) return;

            if (doctor.User != null)
            {
                _context.Users.Remove(doctor.User);
            }
            _context.Doctors.Remove(doctor);
            _context.SaveChanges();
        }
    }
}