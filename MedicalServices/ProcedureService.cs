﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalData;
using MedicalData.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalServices
{
    public class ProcedureService : IProcedure
    {
        private readonly MedicalContext _context;

        public ProcedureService(MedicalContext context)
        {
            _context = context;
        }

        public void Add(Procedure newProcedure)
        {
            _context.Procedures.Add(newProcedure);
            _context.SaveChanges();
        }

        public async void AddAsync(Procedure newProcedure)
        {
            await _context.Procedures.AddAsync(newProcedure);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Procedure> GetAllProcedures()
        {
            var procedures = _context.Procedures;
            return procedures;
        }

        public async Task<IEnumerable<Procedure>> GetAllProceduresAsync()
        {
            return await _context.Procedures.ToListAsync();
        }

        public TimeSpan GetDurationProcedure(string procedureName)
        {
            if (_context.Procedures.Any(x => x.Name == procedureName))
                return _context.Procedures
                    .FirstOrDefault(x => x.Name == procedureName).DurationTime;
            return new TimeSpan(0, 0, 0, 0);
        }

        public async Task<TimeSpan> GetDurationProcedureAsync(string procedureName)
        {
            if (_context.Procedures.Any(x => x.Name == procedureName))
            {
                var durationTime = await _context.Procedures
                    .FirstOrDefaultAsync(x => x.Name == procedureName);
                return durationTime.DurationTime;
            }
            return new TimeSpan(0, 0, 0, 0);
        }

        public Procedure GetProcedure(string name)
        {
            return _context.Procedures
                .FirstOrDefault(a => a.Name == name);
        }

        public async Task<Procedure> GetProcedureAsync(string name)
        {
            return await _context.Procedures
                .FirstOrDefaultAsync(a => a.Name == name);
        }

        public Procedure GetProcedureById(int id)
        {
            return _context.Procedures
                .FirstOrDefault(x => x.Id == id);
        }

        public async Task<Procedure> GetProcedureByIdAsync(int id)
        {
            return await _context.Procedures
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}